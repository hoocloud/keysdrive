#!/usr/bin/env python3
import sys
import os
import logging
import argparse
import subprocess

scriptpath = sys.argv[0]
scriptname = os.path.basename(scriptpath)
progpath = os.path.dirname(os.path.abspath(scriptpath))
progname = os.path.basename(progpath)

logging.basicConfig(level=logging.NOTSET, 
                    format = '%(asctime)s.%(msecs)03d %(levelname)s %(module)s '
                             '%(funcName)s: %(message)s',
                    datefmt = '%Y-%m-%d %H:%M:%S')
log=logging.getLogger()


def main():
    global scriptpath
    global scriptname
    global progpath
    global progname 
    log.debug('scriptpath: \"{}\"'.format(scriptpath))
    log.debug('scriptname: \"{}\"'.format(scriptname))
    log.debug('progpath: \"{}\"'.format(progpath))
    log.debug('progname: \"{}\"'.format(progname))

    parser = argparse.ArgumentParser(prog = progname)
    parser.add_argument('--project', 
                        default = 'default',
                        dest =  'project',
                        help = ('the project'),
                       )
    parser.add_argument('--multibuild', 
                        action="store_true",
                        dest =  'multibuild',
                        help = ('iterate over all folders in the project cfg dir'),
                       )
    args = parser.parse_args()

    project=args.project
    multibuild=args.multibuild

    log.debug('removing container keysdrive-running-{}'.format(project))

    subprocess.run(['podman', 'rm',  '-if',  'keysdrive-running-{}'.format(project)])

    log.debug('starting new keysdrive-running-{}'.format(project))

    if not 'KEYSDRIVE_CFG_DIR' in os.environ:
        os.environ['KEYSDRIVE_CFG_DIR'] = '/srv/containers/prj/{}/{}/cfg'.format(project, progname)
    if not os.path.isdir(os.environ['KEYSDRIVE_CFG_DIR']):
        log.debug('KEYSDRIVE_CFG_DIR directory {} does not exist - exiting'.format(os.environ['KEYSDRIVE_CFG_DIR']))
        sys.exit(1)
    log.debug('KEYSDRIVE_CFG_DIR "{}"'.format(os.environ.get('KEYSDRIVE_CFG_DIR')))

    if not 'KEYSDRIVE_OUT_DIR' in os.environ:
        os.environ['KEYSDRIVE_OUT_DIR'] = '/srv/containers/prj/{}/{}/data/out'.format(project, progname)
    os.makedirs(os.environ['KEYSDRIVE_OUT_DIR'] + '/instances', mode=0o755, exist_ok=True)
    log.debug('KEYSDRIVE_OUT_DIR "{}"'.format(os.environ.get('KEYSDRIVE_OUT_DIR')))

    if not 'KEYSDRIVE_WORK_DIR' in os.environ:
        os.environ['KEYSDRIVE_WORK_DIR'] = '/srv/containers/prj/{}/{}/data/work'.format(project, progname)
    for workdir in [ 'instances', 'ansible']:
        os.makedirs(os.environ['KEYSDRIVE_WORK_DIR'] + '/' + workdir, mode=0o755, exist_ok=True)
    os.makedirs(os.environ['KEYSDRIVE_WORK_DIR'] + '/gnupg', mode=0o700, exist_ok=True)
    log.debug('KEYSDRIVE_WORK_DIR "{}"'.format(os.environ.get('KEYSDRIVE_WORK_DIR')))

    subprocess.run(['podman', 'run',
                    '--rm',
                    '--privileged',
                    '-it',
                    '--name', 'keysdrive-running-{}'.format(project),
                    '--volume', '{}:/cfg'.format(os.environ.get('KEYSDRIVE_CFG_DIR')),
                    '--volume', '{}:/out'.format(os.environ.get('KEYSDRIVE_OUT_DIR')), 
                    '--volume', '{}/instances:/work/instances'.format(os.environ.get('KEYSDRIVE_WORK_DIR')), 
                    '--volume', '{}/gnupg:/root/.gnupg'.format(os.environ.get('KEYSDRIVE_WORK_DIR')), 
                    '--volume', '{}/ansible:/root/ansible'.format(os.environ.get('KEYSDRIVE_WORK_DIR')), 
                    'keysdrive:latest',
                    '/bin/bash',
                   ])

if __name__ == '__main__':
    sys.exit(main())
