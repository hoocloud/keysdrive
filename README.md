## keysdrive

keysdrive creates an image file with keys only for this host
this can be f.e. ssh host keys, client certificates, system user private keys, salt minion keys etc.

the easiest way to create such an image is to use the command:

```
./runKeysdrive
```
